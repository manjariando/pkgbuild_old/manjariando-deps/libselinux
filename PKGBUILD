# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Nicolas Iooss (nicolas <dot> iooss <at> m4x <dot> org)
# Contributor: Timothée Ravier <tim@siosm.fr>
# Contributor: Nicky726 (Nicky726 <at> gmail <dot> com)
# Contributor: Sergej Pupykin (pupykin <dot> s+arch <at> gmail <dot> com)
# Contributor: Zezadas
#
# This PKGBUILD is maintained on https://github.com/archlinuxhardened/selinux.
# If you want to help keep it up to date, please open a Pull Request there.

pkgname=libselinux
_pkgver=3.4
pkgver=${_pkgver/-/}
pkgrel=1.4
pkgdesc="SELinux library and simple utilities"
arch=('x86_64' 'aarch64') # 'i686' 'armv6h')
url='https://github.com/SELinuxProject/selinux'
license=('custom')
groups=('selinux')
makedepends=("libsepol=${pkgver}" 'pkgconf' 'python' 'ruby' 'xz' 'swig')
conflicts=("selinux-usr-${pkgname}" "${pkgname}")
provides=("selinux-usr-${pkgname}=${pkgver}")
source=("https://github.com/SELinuxProject/selinux/releases/download/${_pkgver}/${pkgname}-${_pkgver}.tar.gz"
        "libselinux.tmpfiles.d")
sha256sums=('77c294a927e6795c2e98f74b5c3adde9c8839690e9255b767c5fca6acff9b779'
            'afe23890fb2e12e6756e5d81bad3c3da33f38a95d072731c0422fbeb0b1fa1fc')

build() {
    cd "${srcdir}"

    cd "${pkgname}-${_pkgver}"

    # Do not build deprecated rpm_execcon() interface. It is useless on Arch Linux anyway.
    export DISABLE_RPM=y

    export CFLAGS="${CFLAGS} -fno-semantic-interposition"
    make swigify
    make all
    make PYTHON=/usr/bin/python3 pywrap
    make RUBY=/usr/bin/ruby rubywrap
}

package() {
    depends=("libsepol>=${pkgver}" 'pcre')
    optdepends=('python: python bindings'
                'ruby: ruby bindings')

    cd "${pkgname}-${_pkgver}"

    export DISABLE_RPM=y

    make DESTDIR="${pkgdir}" SBINDIR=/usr/bin SHLIBDIR=/usr/lib install
    make DESTDIR="${pkgdir}" PYTHON=/usr/bin/python3 SBINDIR=/usr/bin SHLIBDIR=/usr/lib install-pywrap
    make DESTDIR="${pkgdir}" RUBY=/usr/bin/ruby SBINDIR=/usr/bin SHLIBDIR=/usr/lib install-rubywrap
    /usr/bin/python3 -m compileall "${pkgdir}/$(/usr/bin/python3 -c 'from distutils.sysconfig import *; print(get_python_lib(plat_specific=1))')"

    install -Dm644 "${srcdir}"/libselinux.tmpfiles.d "${pkgdir}"/usr/lib/tmpfiles.d/libselinux.conf

    # LICENSE
    install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
